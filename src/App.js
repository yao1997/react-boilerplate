import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import MainRouter from "./router/MainRouter.js"
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import {store} from './redux/store.js';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
  },
});

function App() {
  return (
    <Provider store={store} style={{flex:1}}>
      <ThemeProvider theme={theme}>
        <MainRouter/>
      </ThemeProvider>
    </Provider>
  );
}

export default App
