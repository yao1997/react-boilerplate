import React from 'react';
import {connect} from "react-redux";
import {store} from '../redux/store.js';
import {xhrGet,xhrPost} from "../helpers/request";
import {handleLoginCookie} from "../helpers/cookie"
import {auth,alert} from "../redux/actions";
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';
import '../css/home.css';

class LoginPage extends React.Component {
  constructor(props){
    super(props);
    this.state={
        username:'',
        password:'',
        captcha:'',
        captchaImage:""
    }
    this.loadCaptchaHandler=this.loadCaptchaHandler.bind(this);
  }

  componentDidMount(){
    if (this.props.auth){
      return this.props.history.push('/main')
    }
    xhrGet('http://dev.bruida.com/1248/captcha',this.loadCaptchaHandler)
  }

  loadCaptchaHandler(xhr){
    let that=this
    let reader = new FileReader();
    reader.onloadend = function() {
      that.setState({
        ...that.state,
        captchaImage:reader.result,
        captchaSn:xhr.getResponseHeader('Brd-Captcha-Sn')
      })
    }
    reader.readAsDataURL(xhr.response);
  }

  login(){
    let header={}
    const that=this
    let body={
      login:this.state.username,
      pwd:this.state.password,
      captcha:parseInt(this.state.captcha)
    }

    header["brd-captcha-sn"]=this.state.captchaSn
    xhrPost('http://dev.bruida.com/1248/login',body,function(response){
      if (response.msg){
        let username="Test User"
        store.dispatch(auth({name:username}))
        handleLoginCookie(username)
        return that.props.history.push('/main')
      }
      return store.dispatch(alert({
        status:"error",
        title:"Authentication Fail",
        message:"Please check your credentials or captcha."
      }))
    },header)
  }

  render() {
    return (
      <div style={{
        display: 'flex',  justifyContent:'center', alignItems:'center', height: '100vh',
        backgroundImage:`url("https://picjumbo.com/wp-content/uploads/free-stock-photos-san-francisco-1080x720.jpg")`,
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
      }}>
        <form noValidate autoComplete="off" className="login-form">
          <img src="https://upload.wikimedia.org/wikipedia/commons/a/ab/Android_O_Preview_Logo.png" className="logo" alt="logo"/>
           <TextField
             className="input-max-width"
             onChange={(ev)=>{this.setState({...this.state,username:ev.target.value})}}
             variant="filled"
             label="username"
           /><br/>
           <TextField
             className="input-max-width"
             onChange={(ev)=>{this.setState({...this.state,password:ev.target.value})}}
             type="password"
             variant="filled"
             label="password"
           /><br/>

           <img src={this.state.captchaImage} className="captcha" alt="captcha"/><br/>
           <TextField
             label="Captcha"
             className="input-max-width"
             onChange={(ev)=>{this.setState({...this.state,captcha:ev.target.value})}}
             margin="normal"
             variant="filled"
           /><br/>
           <div className="login-btn-container">
             <Fab variant="extended" aria-label="Delete"  className="login-btn"  onClick={()=>this.login()}>
               <NavigationIcon/>Log in
             </Fab>
           </div>
        </form>
        <div style={{position:"absolute",top:0,bottom:0,right:0,left:0,zIndex:1,backgroundColor:`rgba(0,0,0,.7)`}}/>
      </div>
    );
  }
}

function mapStateToProps(storeState,ownProps) {
  return {
    auth:storeState.auth
  }
}

export default connect(mapStateToProps)(LoginPage);
