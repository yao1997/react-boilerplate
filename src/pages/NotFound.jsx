import React from 'react';

export default class NotFound extends React.Component {
  render() {
    return (
      <div className="shopping-list">
        Page Not Found
      </div>
    );
  }
}
