import React from 'react';
import {connect} from "react-redux";
import {store} from "../redux/store.js"
import {logout} from "../redux/actions"
import {handleLogoutCookie} from "../helpers/cookie"
import cookie from 'react-cookies';

class MainPage extends React.Component {
  constructor(props){
    super(props)
  }

  render(){
    return (
      <div className="shopping-list">
        Logged in and the user is:
        <br/>
        {this.props.currentUser?this.props.currentUser.name:""}

        <button onClick={()=>{
          store.dispatch(logout())
          handleLogoutCookie()
          this.props.history.push('/')
        }}>Log out</button>
      </div>
    );
  }
}

function mapStateToProps(storeState,ownProps) {
  return { currentUser:storeState.currentUser }
}

export default connect(mapStateToProps)(MainPage)
