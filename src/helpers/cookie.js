import cookie from 'react-cookies';

export function handleLoginCookie(username){
  cookie.save('name', username, { path: '/' })
  cookie.save('auth',true, { path: '/' })
}

export function handleLogoutCookie(){
  cookie.remove('name')
  cookie.remove('auth')
}
