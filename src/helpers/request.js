import {store} from '../redux/store.js';
import {startLoading,stopLoading} from "../redux/actions.js"

function xhrGet(url, callback) {
   let xhr = new XMLHttpRequest();
   xhr.onload = function() {
     store.dispatch(stopLoading())
     callback(xhr);
   };
   xhr.open('GET', url);
   store.dispatch(startLoading())
   xhr.responseType = 'blob';
   xhr.send();
}

function xhrPost(url,body,callback,headers=null){
  let xhr = new XMLHttpRequest();
  xhr.onload = function() {
    store.dispatch(stopLoading())
    let response=JSON.parse(xhr.response)
    callback(response);
  };
  xhr.open('POST', url);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.setRequestHeader('brd-captcha-sn', headers['brd-captcha-sn']);
  store.dispatch(startLoading())
  xhr.send(JSON.stringify(body));
}

export {xhrGet,xhrPost}
