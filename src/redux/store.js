import { createStore } from 'redux'
import cookie from 'react-cookies';

const isUserLoggedIn=cookie.load('auth')?true:false;

const defaultState={
    isLoading:false,
    auth:isUserLoggedIn,
    currentUser:isUserLoggedIn?{
      name:cookie.load('name')
    }:null,
    alert:null
}

function reducer(state = defaultState, action) {
  switch (action.type) {
    case 'START_LOADING':
      return Object.assign({}, state, {
        isLoading: true
      })
    case "STOP_LOADING":
      return Object.assign({}, state, {
        isLoading: false
      })
    case "AUTH":
      return Object.assign({}, state, {
        auth: true,
        currentUser: action.currentUser
      })
    case "LOGOUT":
      return Object.assign({}, state, {
        auth: false,
        currentUser: null
      })
    case "ALERT":
      return Object.assign({}, state, {
        alert:action.alert
      })
    default:
      return state
  }
}


export let store = createStore(reducer)
