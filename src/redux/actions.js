const START_LOADING = 'START_LOADING';
const STOP_LOADING = "STOP_LOADING";
const AUTH="AUTH";
const LOGOUT="LOGOUT";
const ALERT="ALERT";

export function startLoading() {
  return {
    type: START_LOADING
  }
}

export function stopLoading() {
  return {
    type: STOP_LOADING
  }
}

export function auth(currentUser) {
  return {
    type: AUTH,
    currentUser:currentUser
  }
}

export function logout(currentUser) {
  return {
    type: LOGOUT
  }
}

export function alert(content) {
  return {
    type: ALERT,
    alert:content
  }
}
