import React from 'react';
import { HashRouter,Switch,Route,Redirect } from 'react-router-dom';
import LoginPage from "../pages/LoginPage.jsx";
import MainPage from "../pages/MainPage.jsx";
import NotFound from "../pages/NotFound.jsx";
import CircularProgress from '@material-ui/core/CircularProgress';
import SweetAlert from "react-bootstrap-sweetalert"
import {connect} from "react-redux";
import {alert} from "../redux/actions"
import {store} from '../redux/store.js';

class MainRouter extends React.Component {
  render(){
    return(
      <HashRouter style={{flex:1}}>
          <Switch style={{flex:1}}>
              <Route path="/" exact component={LoginPage} style={{flex:1}}/>
              <Route path="/main" exact
                render={props => {
                  return this.props.auth?   <Route component={MainPage}/>:<Redirect to="/" />
                }}
              />
              <Route component={NotFound}/>
          </Switch>
          <div style={{...loaderStyle,visibility:this.props.isLoading?"visible":"hidden"}}>
            <CircularProgress size={60}/>
          </div>
          <SweetAlert type={this.props.alert?this.props.alert.status:"success"} title={this.props.alert?this.props.alert.title:""} show={this.props.alert?true:false}
            onConfirm={()=>{store.dispatch(alert(null))}}
          >
              {this.props.alert?this.props.alert.message:""}
          </SweetAlert>
      </HashRouter>
    )
  }
}

const loaderStyle={
  position:'absolute',
  top:0,bottom:0,right:0,left:0,
  zIndex:100,display:"flex",
  justifyContent:"center",alignItems:"center",
  backgroundColor:`rgba(0,0,0,.7)`
}

function mapStateToProps(storeState,ownProps) {
  return {
    isLoading:storeState.isLoading,
    alert:storeState.alert,
    auth:storeState.auth
  }
}

export default connect(mapStateToProps)(MainRouter)
